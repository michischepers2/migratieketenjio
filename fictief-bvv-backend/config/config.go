package config

import (
	"fmt"

	"github.com/iamolegga/enviper"
	"github.com/spf13/viper"
)

type Config struct {
	BackendListenAddress string
	PostgresDSN          string `mapstructure:"POSTGRES_DSN"`
}

func New(configPath string) (*Config, error) {
	e := enviper.New(viper.New())
	e.SetEnvPrefix("MK")

	e.SetDefault("BACKENDLISTENADDRESS", ":8080")

	config := new(Config)

	if err := e.Unmarshal(config); err != nil {
		panic("unable to decode into config struct")
	}

	fmt.Printf("config: %v\n", config)

	return config, nil
}
