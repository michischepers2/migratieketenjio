package cmd

import "github.com/spf13/cobra"

const (
	PSQLSchemaName = "bvv_backend"
)

var RootCmd = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "backend",
	Short: "BVV backend",
	Long:  "Fictief basis voorziening vreemdelingen",
}

func Execute() error {
	return RootCmd.Execute() //nolint:wrapcheck // not necessary
}

//nolint:gochecknoinits // this is the recommended way to use cobra
func init() {
	RootCmd.AddCommand(serveCommand)
	RootCmd.AddCommand(migrateCommand)
}
